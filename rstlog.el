(require 'subr-x) ;; For string-trim

(setenv "PYTHONPATH" (shell-command-to-string "bash --login -c 'echo -n $PYTHONPATH'"))

(defconst rstlog_path (file-name-directory (or load-file-name buffer-file-name)))
(defconst rstlog-summarize (format "%srstlog/summarize.py" rstlog_path))
(defconst rstlog-sync-wrike (format "%srstlog/syncwrike.py" rstlog_path))
(defconst rstlog-wrike (format "%srstlog/wrike.py" rstlog_path))

(defvar wrike-buffer "*Wrike*")

(defvar log-file (format "/home/%s/Documents/log/log.rst" (getenv "USER")))
(defvar log-summary (format "/home/%s/Documents/log/log_summary.txt" (getenv "USER")))
(defvar log-mirror nil)
;; (defvar log-summary (format "/shared/home/%s/Documents/log_summary.txt" (getenv "USER")))
;; (defvar log-mirror (format "/shared/home/%s/Documents/log.rst" (getenv "USER")))
(defvar wrike-username user-mail-address)
(defvar wrike-current-project-folder-v2id nil)
(defvar wrike-projects-folder-v2id nil)
(defvar wrike-pr-folder-v2id nil)
(defvar wrike-misc-folder-v2id nil)
(defvar wrike-consultation-task-v2id nil)
(defvar wrike-meeting-task-v2id nil)
(defvar wrike-home-folder-v2id nil)

(defconst rstlog-sync-jira (format "%srstlog/syncjira.py" rstlog_path))
(defvar jira-buffer "*Jira*")
(defvar jira-username "daxline")

(defun open-log ()
  "Open work log."
  (interactive)
  (switch-to-buffer (find-file-noselect log-file))
  (add-hook 'after-save-hook 'summarize-and-upload-log nil t))


(defun open-log-summary ()
  "Open work log summary."
  (interactive)
  (switch-to-buffer (find-file-noselect log-summary)))

(defun open-rstlog-messages ()
  "Open rstlog messages."
  (interactive)
  (switch-to-buffer "*rstlog*"))

(defun write-log-summary ()
  "Write a summary of the log file."
  (interactive)
  (shell-command
   (format "python3 %s %s %s" rstlog-summarize log-file log-summary) nil nil)
  )

(defun async-write-log-summary ()
  "Asynchronously write a summary of the log file."
  (interactive)
  (with-current-buffer (get-buffer-create "*rstlog*")
    (goto-char (point-max))
    (insert-utc-time)
    (insert "\n"))
  (start-process-shell-command "async-write-log-summary"
                               (get-buffer-create "*rstlog*")
                               (format "python3 %s %s %s" rstlog-summarize log-file log-summary)))

(defun mirror-log ()
  "Upload the log to the powers that be."
  (interactive)
  (if log-mirror
      (progn
        (delete-file log-mirror)
        (copy-file log-file log-mirror))))

(defun summarize-and-upload-log ()
  "Asynchronously upload the log to the powers that be."
  (interactive)
  (with-current-buffer (get-buffer-create "*rstlog*")
    (goto-char (point-max))
    (insert "\n")
    (insert-utc-time)
    (insert "\n"))
  (if log-mirror
    (start-process-shell-command
     "summarize-and-upload-log"
     (get-buffer-create "*rstlog*")
     (format "python3 \"%s\" \"%s\" \"%s\"; rm \"%s\"; cp \"%s\" \"%s\" && echo -n \"Log mirrored\""
             (expand-file-name rstlog-summarize)
             (expand-file-name log-file)
             (expand-file-name log-summary)
             (expand-file-name log-mirror)
             (expand-file-name log-file)
             (expand-file-name log-mirror))))
  )


(defun sync-wrike ()
  "Synchronize Wrike task timelogs with times in work log"
  (interactive)
  (with-current-buffer (get-buffer-create wrike-buffer)
    (switch-to-buffer wrike-buffer)
    (erase-buffer)
    (shell-command
     (format "python3 %s report %s %s" rstlog-sync-wrike wrike-username log-file)
     (get-buffer-create wrike-buffer)
     )
    (if (y-or-n-p "Synchronize Wrike?")
        (start-process "syncwrike"
                       (get-buffer-create wrike-buffer)
                       "python" rstlog-sync-wrike "sync" wrike-username log-file)
      nil)
    )
  )

(defun sync-jira ()
  "Synchronize Jira task timelogs with times in work log"
  (interactive)
  (with-current-buffer (get-buffer-create jira-buffer)
    (switch-to-buffer jira-buffer)
    (erase-buffer)
    (shell-command
     (format "python3 %s report %s %s" rstlog-sync-jira jira-username log-file)
     (get-buffer-create jira-buffer)
     )
    (if (y-or-n-p "Synchronize Jira?")
        (start-process "syncjira"
                       (get-buffer-create jira-buffer)
                       "python3" rstlog-sync-jira "sync" jira-username log-file)
      nil)
    )
  )

(defun insert-wrike-task-id ()
  (interactive)
  (save-excursion
    (next-line)
    (beginning-of-line)
    (rst-backward-section 1)
    (set 'task-title (string-trim (thing-at-point 'line t)))
    (with-current-buffer (get-buffer-create wrike-buffer)
      (erase-buffer)
      (shell-command
       (format "python3 %s %s find_task_v2id \"%s\""
               rstlog-wrike wrike-username task-title)
       (get-buffer-create wrike-buffer)
       )
      (set 'task-id (format "W%s: " (string-trim (thing-at-point 'line t))))
      )
    (insert task-id)
    (rst-adjust nil))
  )

(defun insert-date ()
  "Insert current date yyyy-mm-dd."
  (interactive)
  (when (use-region-p)
    (delete-region (region-beginning) (region-end) )
    )
  (insert (format-time-string "%Y-%m-%d"))
  )

(defun insert-time ()
  "Insert current 24-hour time HH:MM."
  (interactive)
  (when (use-region-p)
    (delete-region (region-beginning) (region-end) )
    )
  (insert (format-time-string "%H:%M"))
  )

(defun utc-time ()
  (concat
   (format-time-string "%Y-%m-%dT%T")
   ((lambda (x) (concat (substring x 0 3) ":" (substring x 3 5)))
    (format-time-string "%z")))
  )

(defun utc-time-plus-8-hours ()
  (concat
   (format-time-string "%Y-%m-%dT")
   (number-to-string (+ 8 (string-to-number (format-time-string "%H"))))
   (format-time-string ":%M:%S")
   ((lambda (x) (concat (substring x 0 3) ":" (substring x 3 5)))
    (format-time-string "%z")))
  )

(defun insert-utc-time ()
  "Insert current UTC time"
  (interactive)
  (when (use-region-p)
    (delete-region (region-beginning) (region-end) )
    )
  (insert (utc-time))
  )

;; It would be nice if mark-begin-work and mark-end-work set here/away
;; in irc.  Would be nice if we also had separate here/away commands for
;; irc for on-the-clock away-from-irc.

;; insert time marker
(defun mark-begin-work ()
  "Insert work log section timestamp."
  (interactive)
  (let ((start_pos (point))
        )
    (rst-backward-section 1)
    (next-line 2)
    (beginning-of-line)
    (insert ".. timestamp:: ")
    (insert-utc-time)
    (insert "\n")
    (goto-char (+ start_pos 41)) ;; 41 is length of timestamp
    )
  )

;; insert stop-work timestamp
(defun mark-end-work ()
  "Insert stop-work timestamp."
  (interactive)
  (let ((start_pos (point))
        )
    (rst-backward-section 1)
    (next-line 2)
    (beginning-of-line)
    (insert ".. timestamp:: ")
    (insert-utc-time)
    (insert "\n")
    (insert "    :stop:")
    (insert "\n")
    (goto-char (+ start_pos 52)) ;; 52 is length of timestamp
    )
  )

(defun mark-end-8-hours ()
  "Insert stop-work timestamp."
  (interactive)
  (let ((start_pos (point))
        )
    (rst-backward-section 1)
    (next-line 2)
    (beginning-of-line)
    (insert ".. timestamp:: ")
    (insert (utc-time-plus-8-hours))
    (insert "\n")
    (insert "    :stop:")
    (insert "\n")
    (goto-char (+ start_pos 52)) ;; 52 is length of timestamp
    )
  )

;; insert date heading
(defun mark-date-section ()
  "Insert date section."
  (interactive)
  (insert-date)
  ;; (1- (length (thing-at-point `line t)))
  (let ((len (current-column)))
    (insert "\n")
    (insert-char ?= len))
  (insert "\n")
  (mark-begin-work)
  (mark-end-8-hours)
  )


(defun mark-section ()
  "Insert section with timestamp"
  (interactive)
  (let ((title (read-string "Section title: " "" nil nil))
        )
    (insert title)
    (insert "\n")
    (insert-char ?- (length title))
    (insert "\n")
    (mark-begin-work)
    (insert "\n")
    )
  )

(defun mark-pr-section ()
  "Insert PR section with timestamp"
  (interactive)
  (let ((title (read-string "Section title: " "PR" nil nil))
        )
    (insert title)
    (insert "\n")
    (insert-char ?- (length title))
    (insert "\n")
    (mark-begin-work)
    (insert "\n")
    )
  )

(defun mark-consultation-section ()
  "Insert consultation section with timestamp"
  (interactive)
  (let ((title (read-string "Section title: " "Consultation: " nil nil))
        )
    (insert title)
    (insert "\n")
    (insert-char ?- (length title))
    (insert "\n")
    (mark-begin-work)
    (insert "\n")
    )
  )

(defun search-date-section (direction)
  (re-search-forward "\n[0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}.*\n=\\{10\\}\n" nil nil direction))

(defun search-date-section-forward ()
  "Move point to immedately after next date section heading."
  (interactive)
  (search-date-section 1))

(defun search-date-section-backward ()
  "Move point to immediately after the previous/current date section heading."
  (interactive)
  (search-date-section -1)
  (forward-char)
  (next-line 2)
  (beginning-of-line))

(defun current-date-section ()
  "Get the point immediately after the previous/current date section heading."
  (interactive)
  (save-excursion
    (search-date-section-backward)
    (point)))

(defun next-date-section ()
  "Get the point immediately after the next date section heading."
  (interactive)
  (save-excursion
    (search-date-section-forward)
    (point)))

(defun print-day-start ()
  (interactive)
  (message "day-start %d" (current-date-section))
  )

(defun date-section-range ()
  (list
   (current-date-section)
   (condition-case nil
       (next-date-section)
     (error (point-max)))))

(defun search-timestamp ()
  (interactive)
  ;;(re-search-forward "\n\.\. timestamp\:\:[0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}T[0-9]\\{2\\}[0-9]\\{2\\}[0-9]\\{2\\}-[0-9]\\{2\\}\:[0-9]\\{2\\}\n" nil nil 1)
  (re-search-forward "\n\.\. timestamp\:\: [0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}T[0-9]\\{2\\}\:[0-9]\\{2\\}\:[0-9]\\{2\\}-[0-9]\\{2\\}\:[0-9]\\{2\\}\n"
                     nil nil 1)
  )

(defun test-timestamp-search ()
  (interactive)
  (re-search-forward
   "\.\. timestamp\:\: [0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}T[0-9]\\{2\\}\:[0-9]\\{2\\}\:[0-9]\\{2\\}-[0-9]\\{2\\}\:[0-9]\\{2\\}"
   nil
   t)
  (message (match-string 0)))

;; For some reason string-greaterp doesn't exist even though emacs manual says it does??!
(defun string> (a b)
  (string< b a))

(defun find-timestamps (start end)
  (interactive)
  (save-excursion
    (goto-char start)
    (let (searchresults lastpoint)
      (while (not (eq lastpoint (point)))
        (setq lastpoint (point))
        (while (re-search-forward
                "\n\.\. timestamp\:\: [0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}T[0-9]\\{2\\}\:[0-9]\\{2\\}\:[0-9]\\{2\\}-[0-9]\\{2\\}\:[0-9]\\{2\\}\n"
                end
                t)
          ;;(setq searchresults (cons (match-string 0 nil) searchresults)))
          (setq searchresults (cons (list (match-string 0 nil) (point)) searchresults)))
        )
      searchresults)))


;; Use this when you are frequently switching between two tasks.
(defun mark-last-section-begin-work ()
  "Insert start-work timestamp on previous section"
  (interactive)
  (goto-char (cadadr (sort (apply 'find-timestamps (date-section-range))
                           '(lambda (l r) (string> (car l) (car r))))))
  (mark-begin-work))

  ;; (message (caar (sort (apply 'find-timestamps (date-section-range))
  ;;                     '(lambda (l r) (string> (car l) (car r)))))))



;; (defun mark-note-section ()
;;   "Insert executive-summary note section"
;;   (interactive)
;;   )

;; (defun mark-restart-work ()
;;   "Restart work from previous day"
;;   (interactive)
;;   )

(defun rstlog-add-wrike-task (default_title folder-v2id-var)
  (let ((task-title (read-string "Task title: " default_title nil nil))
        )
    (with-current-buffer (get-buffer-create wrike-buffer)
      (erase-buffer)
      (shell-command
       (format "python3 %s %s add_task \"%s\" %s"
               rstlog-wrike wrike-username task-title (symbol-value folder-v2id-var))
       (get-buffer-create wrike-buffer)
       )
      (set 'task-id (string-trim (thing-at-point 'line t)))
      )
    (insert "W")
    (insert task-id)
    (insert ": ")
    (insert task-title)
    (insert "\n")
    (next-line -1)
    (beginning-of-line)
    (set 'title-len (+ -1 (length (thing-at-point 'line t))))
    (next-line)
    (beginning-of-line)
    (insert-char ?- title-len)
    (insert "\n")
    (mark-begin-work)
    (insert "\n")))

(defun new-rstlog-add-wrike-task (folder-v2id-var)
  (save-restriction
  (save-excursion
    (next-line)
    (beginning-of-line)
    (rst-backward-section 1)
    (set 'default_title (string-trim (thing-at-point 'line t)))
    (next-line 2)
    (home-end-home)
    (set 'descr-start (point))
    (rst-forward-section 1)
    (set 'descr-end (point))
    (set 'descr (buffer-substring descr-start descr-end))
    )

  (let ((task-title (read-string "Task title: " default_title nil nil))
        )
    (with-current-buffer (get-buffer-create wrike-buffer)
      (erase-buffer)
      (shell-command
       (format "python3 %s %s add_task \"%s\" %s"
               rstlog-wrike wrike-username task-title (symbol-value folder-v2id-var))
       (get-buffer-create wrike-buffer)
       )
      (set 'task-id (string-trim (thing-at-point 'line t)))
      )
    (insert "W")
    (insert task-id)
    (insert ": ")
    (insert task-title)
    (insert "\n")
    (next-line -1)
    (beginning-of-line)
    (set 'title-len (+ -1 (length (thing-at-point 'line t))))
    (next-line)
    (beginning-of-line)
    (insert-char ?- title-len)
    (insert "\n")
    (mark-begin-work)
    (insert "\n"))))



(defun rstlog-add-wrike-pr-task ()
  "Insert section for a new PR-related wrike task."
  (interactive)
  (rstlog-add-wrike-task "PR" 'wrike-pr-folder-v2id))

(defun rstlog-add-wrike-misc-task ()
  "Insert section for a new misc wrike task."
  (interactive)
  (rstlog-add-wrike-task "" 'wrike-misc-folder-v2id))

(defun rstlog-add-wrike-current-project-task ()
  "Insert section for a new current-project wrike task."
  (interactive)
  (rstlog-add-wrike-task "" 'wrike-current-project-folder-v2id))

(defun rstlog-add-wrike-folder ()
  "Create a new folder in wrike projects folder and set it as the current project."
  (interactive)
  (let ((folder-title (read-string "Folder title: " "" nil nil))
        )
    (with-current-buffer (get-buffer-create wrike-buffer)
      (erase-buffer)
      (shell-command
       (format "python3 %s %s add_folder \"%s\" %s"
               rstlog-wrike
               wrike-username
               folder-title
               wrike-projects-folder-v2id)
       (get-buffer-create wrike-buffer)
       )
      (setq wrike-current-project-folder-v2id (string-trim (thing-at-point 'line t)))
      )
  ))


(require 'browse-url)
(defun rstlog-browse-wrike (v2id)
  (browse-url (format "https://www.wrike.com/open.htm?id=%s" v2id))
  )
(defun rstlog-browse-current-wrike-folder ()
  (interactive)
  (rstlog-browse-wrike wrike-current-project-folder-v2id))

(defun rstlog-browse-current-wrike-task ()
  (interactive)
  (save-excursion
    (next-line)
    (beginning-of-line)
    (rst-backward-section 1)
    (set 'task-title (string-trim (thing-at-point 'line t)))
    (with-current-buffer (get-buffer-create wrike-buffer)
      (erase-buffer)
      (shell-command
       (format "python3 %s %s find_task_v2id \"%s\""
               rstlog-wrike
               wrike-username
               task-title)
       (get-buffer-create wrike-buffer)
       )
      (rstlog-browse-wrike (string-trim (thing-at-point 'line t)))
      )))

(defun rstlog-print-current-wrike-folder-tree ()
  "Print the names and ids of the current wrike project folder and subfolders."
  (interactive)
  (rstlog-get-wrike-folders wrike-current-project-folder-v2id)
  )

(defun rstlog-print-current-wrike-folder ()
  "Get the name of the current wrike folder."
  (interactive)
  (rstlog-get-wrike-folder wrike-current-project-folder-v2id))

(defun rstlog-set-current-wrike-folder ()
  "Set the folder new tasks will be added to when added with M-x rstlog-add-wrike-current-project-task."
  (interactive)
  (set 'v2id (rstlog-select-wrike-folder wrike-home-folder-v2id))
  (if v2id
      (setq wrike-current-project-folder-v2id v2id)
    ))

(defun rstlog-select-wrike-folder (parent-folder-v2id)
  (set 'folders_alist (cons
                       (car (rstlog-get-wrike-folder parent-folder-v2id))
                       (rstlog-get-wrike-folders parent-folder-v2id)))

  (set 'selection (completing-read
                   "Select folder: "
                   folders_alist
                   nil
                   t
                   nil
                   nil
                   nil))
  (if selection
      (cdr (assoc selection folders_alist))
    ))

(defun rstlog-get-wrike-folders (parent-folder-v2id)
  "Get an associative array of folder titles and folder v2ids inside the parent folder."
  (set 'folders nil)
  (with-current-buffer (get-buffer-create wrike-buffer)
    (erase-buffer)
    (shell-command
     (format "python3 %s %s get_folders %s"
             rstlog-wrike
             wrike-username
             parent-folder-v2id)
     (get-buffer-create wrike-buffer)
     )
    (goto-char (point-min))
    (while (not (eobp))
      (let ((line (buffer-substring (point)
                                    (progn (forward-line 1) (point)))))
        (string-match "^\\([0-9]+\\) \\(.*\\)" line)
        (set 'folders (cons (cons (match-string 2 line)
                                  (match-string 1 line))
                            folders))
        )
      )
    )
  (symbol-value 'folders)
  )

(defun rstlog-get-wrike-folder (parent-folder-v2id)
  "Get a cons cell with folder title and folder v2id."
  (set 'folders nil)
  (with-current-buffer (get-buffer-create wrike-buffer)
    (erase-buffer)
    (shell-command
     (format "python3 %s %s get_folder %s"
             rstlog-wrike
             wrike-username
             parent-folder-v2id)
     (get-buffer-create wrike-buffer)
     )
    (goto-char (point-min))
    (while (not (eobp))
      (let ((line (buffer-substring (point)
                                    (progn (forward-line 1) (point)))))
        (string-match "^\\([0-9]+\\) \\(.*\\)" line)
        (set 'folders (cons (cons (match-string 2 line)
                                  (match-string 1 line))
                            folders))
        )
      )
    )
  (symbol-value 'folders)
  )

(defun rstlog-get-wrike-task-status ()
  "Get the status of the task where point is."
  (interactive)
  (save-excursion
    (next-line)
    (beginning-of-line)
    (rst-backward-section 1)
    (set 'task-title (string-trim (thing-at-point 'line t)))
    (with-current-buffer (get-buffer-create wrike-buffer)
      (erase-buffer)
      (shell-command
       (format "python3 %s %s get_status \"%s\""
               rstlog-wrike
               wrike-username
               task-title)
       (get-buffer-create wrike-buffer)
       )
      (message (string-trim (thing-at-point 'line t)))
      )))

(defun rstlog-set-wrike-task-status ()
  "Set the status of the task where point is."
  (interactive)
  (set 'status (completing-read
                "Select status: "
                '("Active" "Completed" "Cancelled")))
  (if status
      (save-excursion
        (next-line)
        (beginning-of-line)
        (rst-backward-section 1)
        (set 'task-title (string-trim (thing-at-point 'line t)))
        (with-current-buffer (get-buffer-create wrike-buffer)
          (erase-buffer)
          (shell-command
           (format "python3 %s %s set_status \"%s\" %s"
                   rstlog-wrike
                   wrike-username
                   task-title
                   status)
           (get-buffer-create wrike-buffer)
           )
          (message status)
          ))))


(global-set-key (kbd "C-c ll") 'open-log)
(global-set-key (kbd "C-c ls") 'open-log-summary)
(global-set-key (kbd "C-c lm") 'open-rstlog-messages)
(global-set-key (kbd "C-c id") 'insert-date)
(global-set-key (kbd "C-c it") 'insert-utc-time)
;; (define-key rst-mode-map (kbd "C-c wsy") 'sync-wrike)
;; (define-key rst-mode-map (kbd "C-c wap") 'rstlog-add-wrike-pr-task)
;; (define-key rst-mode-map (kbd "C-c wam") 'rstlog-add-wrike-misc-task)
;; (define-key rst-mode-map (kbd "C-c wat") 'rstlog-add-wrike-current-project-task)
;; (define-key rst-mode-map (kbd "C-c waf") 'rstlog-add-wrike-folder)
;; (define-key rst-mode-map (kbd "C-c wpf") 'rstlog-print-current-wrike-folder)
;; (define-key rst-mode-map (kbd "C-c wsf") 'rstlog-set-current-wrike-folder)
;; (define-key rst-mode-map (kbd "C-c wgs") 'rstlog-get-wrike-task-status)
;; (define-key rst-mode-map (kbd "C-c wss") 'rstlog-set-wrike-task-status)
;; (define-key rst-mode-map (kbd "C-c wbf") 'rstlog-browse-current-wrike-folder)
;; (define-key rst-mode-map (kbd "C-c wbt") 'rstlog-browse-current-wrike-task)

(define-key rst-mode-map (kbd "C-c js") 'sync-jira)

(define-key rst-mode-map (kbd "C-c mb") 'mark-begin-work)
(define-key rst-mode-map (kbd "C-c mc") 'mark-consultation-section)
(define-key rst-mode-map (kbd "C-c md") 'mark-date-section)
(define-key rst-mode-map (kbd "C-c me") 'mark-end-work)
(define-key rst-mode-map (kbd "C-c ml") 'mark-last-section-begin-work)
;;(define-key rst-mode-map (kbd "C-c mn") 'mark-note-section)
(define-key rst-mode-map (kbd "C-c mp") 'mark-pr-section)
;;(define-key rst-mode-map (kbd "C-c mr") 'mark-restart-work)
(define-key rst-mode-map (kbd "C-c ms") 'mark-section)
(define-key rst-mode-map (kbd "C-c wt") 'insert-wrike-task-id)

(define-key rst-mode-map (kbd "C-<") 'rst-backward-section)
(define-key rst-mode-map (kbd "C->") 'rst-forward-section)

(define-key rst-mode-map (kbd "C-c <") 'indent-rigidly-left-to-tab-stop)
(define-key rst-mode-map (kbd "C-c >") 'indent-rigidly-right-to-tab-stop)

(define-key rst-mode-map (kbd "C-c tc") 'table-capture)
(define-key rst-mode-map (kbd "C-c tf") 'table-release)
(define-key rst-mode-map (kbd "C-c ti") 'table-insert)
(define-key rst-mode-map (kbd "C-c tr") 'table-recognize)
(define-key rst-mode-map (kbd "C-c tu") 'table-unrecognize)

;; Other useful keyboard combos:
;; C-c = 'rst-adjust  (or one of those commands that fix section titles/adornments)

