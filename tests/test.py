from wryke.wrike import Wrike, v2id, url_id
import os
import rstlog.syncwrike
import rstlog.wrike
import sys
import tempfile
import unittest


class TestRSTLog(unittest.TestCase):
    def __init__(self, testname, user, folder_v2_id):
        super(TestRSTLog, self).__init__(testname)
        self.user = user
        self.test_folder_v2id = folder_v2id

    def setUp(self):
        self.wrk = Wrike()
        self.test_folder_v3id = self.wrk.find_v3id(self.test_folder_v2id,
                                                   'ApiV2Folder')

    def test_add_folder(self):
        folder_v2id = rstlog.wrike.add_folder(self.user, "Test folder",
                                              self.test_folder_v2id)
        try:
            int(folder_v2id)
        except ValueError:
            valid_folder_id = False
        else:
            valid_folder_id = True
        self.assertEqual(valid_folder_id, True)

        folder_v3id = self.wrk.find_v3id(folder_v2id, 'ApiV2Folder')
        user = self.wrk.find_contact(self.user)
        folder = self.wrk.folders[folder_v3id]()[0]
        self.assertEqual(user['id'] in folder['sharedIds'], True)

        self.wrk.folders[folder_v3id].delete()

    def test_add_task(self):
        task_v2id = rstlog.wrike.add_task(self.user, "Test task",
                                          self.test_folder_v2id)
        try:
            int(task_v2id)
        except ValueError:
            valid_task_id = False
        else:
            valid_task_id = True
        self.assertEqual(valid_task_id, True)

        if valid_task_id:
            task_v3id = self.wrk.find_v3id(task_v2id, 'ApiV2Task')
            user = self.wrk.find_contact(self.user)
            task = self.wrk.tasks[task_v3id]()[0]
            self.wrk.tasks[task_v3id].delete()
            self.assertEqual(user['id'] in task['responsibleIds'], True)


    def test_find_task_v2id_by_pr(self):
        user = self.wrk.find_contact(self.user)
        task = self.wrk.create_task(folder_id=self.test_folder_v3id,
                                    title='PR999999: test_create_task',
                                    responsibles=[str(user['id'])])
        task_v3id = task['id']
        task_v2id = v2id(task)
        f1 = rstlog.wrike.find_task_v2id(self.user, 'PR999999: test_create_task')
        f2 = rstlog.wrike.find_task_v2id(self.user, 'PR999999')
        self.wrk.tasks[task_v3id].delete()
        self.assertEqual(f1, task_v2id)
        self.assertEqual(f2, task_v2id)

    def test_find_task_v2id_by_exact_title(self):
        user = self.wrk.find_contact(self.user)
        task = self.wrk.create_task(folder_id=self.test_folder_v3id,
                                    title='test_create_task',
                                    responsibles=[str(user['id'])])
        task_v3id = task['id']
        task_v2id = v2id(task)
        f1 = rstlog.wrike.find_task_v2id(self.user, 'test_create_task')
        self.wrk.tasks[task_v3id].delete()
        self.assertEqual(f1, task_v2id)

    def test_find_task_v2id_by_id(self):
        ''' Really just testing find_task_by_title with v2id.
        Should test this in wryke instead.
        '''
        user = self.wrk.find_contact(self.user)
        task = self.wrk.create_task(folder_id=self.test_folder_v3id,
                                    title='test_create_task',
                                    responsibles=[str(user['id'])])
        task_v3id = task['id']
        task_v2id = v2id(task)
        f1 = rstlog.wrike.find_task_v2id(self.user, 'W%s:' % task_v2id)
        self.wrk.tasks[task_v3id].delete()
        self.assertEqual(f1, task_v2id)

    def test_sync_exact_title(self):
        log_file = tempfile.NamedTemporaryFile(suffix='.rst')
        log_file.write(
'''
2019-01-23
==========
.. timestamp:: 2019-01-23T18:43:37-06:00
    :stop:

test_sync_exact_title
---------------------
.. timestamp:: 2019-01-23T10:43:37-06:00

''')
        log_file.flush()
        user = self.wrk.find_contact(self.user)
        task = self.wrk.create_task(folder_id=self.test_folder_v3id,
                                    title='test_sync_exact_title',
                                    responsibles=[str(user['id'])])
        task_v3id = task['id']

        rstlog.syncwrike.syncwrike('sync', self.user, log_file.name)
        timelogs = self.wrk.tasks[task_v3id].timelogs()

        # Sync again to make sure nothing changes.
        rstlog.syncwrike.syncwrike('sync', self.user, log_file.name)
        timelogs2 = self.wrk.tasks[task_v3id].timelogs()

        self.wrk.tasks[task_v3id].delete()

        self.assertEqual(len(timelogs), 1)
        self.assertEqual(timelogs[0]['hours'], 8)
        self.assertEqual(len(timelogs2), 1)
        self.assertEqual(timelogs2[0]['hours'], 8)



    def test_sync_pr_title(self):
        log_file = tempfile.NamedTemporaryFile(suffix='.rst')
        log_file.write(
'''
2019-01-23
==========
.. timestamp:: 2019-01-23T18:43:37-06:00
    :stop:

PR999999999: blargh
-------------------
.. timestamp:: 2019-01-23T14:43:37-06:00

PR999999999
-----------
.. timestamp:: 2019-01-23T10:43:37-06:00

''')
        log_file.flush()
        user = self.wrk.find_contact(self.user)
        task = self.wrk.create_task(folder_id=self.test_folder_v3id,
                                    title='PR999999999: test_sync_pr_title',
                                    responsibles=[str(user['id'])])
        task_v3id = task['id']

        rstlog.syncwrike.syncwrike('sync', self.user, log_file.name)
        timelogs = self.wrk.tasks[task_v3id].timelogs()

        # Sync again to make sure nothing changes.
        rstlog.syncwrike.syncwrike('sync', self.user, log_file.name)
        timelogs2 = self.wrk.tasks[task_v3id].timelogs()

        self.wrk.tasks[task_v3id].delete()


        self.assertEqual(len(timelogs), 2)
        self.assertEqual(timelogs[0]['hours'], 4)
        self.assertEqual(timelogs[1]['hours'], 4)
        self.assertEqual(len(timelogs2), 2)
        self.assertEqual(timelogs2[0]['hours'], 4)
        self.assertEqual(timelogs2[1]['hours'], 4)


    def test_sync_v2id_title(self):
        user = self.wrk.find_contact(self.user)
        task = self.wrk.create_task(folder_id=self.test_folder_v3id,
                                    title='PR999999999: test_sync_pr_title',
                                    responsibles=[str(user['id'])])
        task_v3id = task['id']

        log_file = tempfile.NamedTemporaryFile(suffix='.rst')
        log_file.write(
'''
2019-01-23
==========
.. timestamp:: 2019-01-23T18:43:37-06:00
    :stop:

W%s: blargh
-------------------
.. timestamp:: 2019-01-23T10:43:37-06:00

''' % v2id(task))
        log_file.flush()

        rstlog.syncwrike.syncwrike('sync', self.user, log_file.name)
        timelogs = self.wrk.tasks[task_v3id].timelogs()

        # Sync again to make sure nothing changes.
        rstlog.syncwrike.syncwrike('sync', self.user, log_file.name)
        timelogs2 = self.wrk.tasks[task_v3id].timelogs()

        self.wrk.tasks[task_v3id].delete()

        self.assertEqual(len(timelogs), 1)
        self.assertEqual(timelogs[0]['hours'], 8)
        self.assertEqual(len(timelogs2), 1)
        self.assertEqual(timelogs2[0]['hours'], 8)


    # def test_sync_consultation(self):
    #     pass

    # def test_sync_meeting(self):
    #     pass



if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('''Usage: test.py <email> <permalink (ApiV2) id of test folder in Wrike>
eg: test.py myemail@nowhere.net 309823175''')
        exit(0)
    user = sys.argv[1]
    folder_v2id = sys.argv[2]

    test_loader = unittest.TestLoader()
    test_names = test_loader.getTestCaseNames(TestRSTLog)

    suite = unittest.TestSuite()
    for test_name in test_names:
        suite.addTest(TestRSTLog(test_name, user, folder_v2id))

    result = unittest.TextTestRunner().run(suite)
    sys.exit(not result.wasSuccessful())
