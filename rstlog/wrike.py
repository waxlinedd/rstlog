from __future__ import print_function
from wryke.wrike import Wrike, v2id
from syncwrike import syncwrike
import sys

def find_task_v2id(username, task_title):
    wrk = Wrike()
    user = wrk.find_contact(username)
    task = wrk.find_task_by_title(task_title, user['id'])
    if task:
        return v2id(task)
    return ''

def add_task(username, task_title, folder_v2id):
    wrk = Wrike()
    user = wrk.find_contact(username)
    folder_v3id = wrk.find_v3id(folder_v2id, 'ApiV2Folder')
    task = wrk.folders[folder_v3id].tasks.post(
        title=task_title,
        responsibles=[str(user['id'])])[0]
    return v2id(task)

def add_folder(username, folder_title, parent_folder_v2id):
    wrk = Wrike()
    user = wrk.find_contact(username)
    parent_folder_v3id = wrk.find_v3id(parent_folder_v2id, 'ApiV2Folder')
    folder = wrk.folders[parent_folder_v3id].folders.post(
        title=folder_title)[0]
    return v2id(folder)

def get_folders(username, parent_folder_v2id):
    wrk = Wrike()
    user = wrk.find_contact(username)
    parent_folder_v3id = wrk.find_v3id(parent_folder_v2id, 'ApiV2Folder')
    folders_lite = wrk.folders[parent_folder_v3id].folders()
    folders_v3id = [f['id'] for f in folders_lite]

    # If v3ids were ok:
    # folders_id_title = [(f['id'], f['title']) for f in folders_lite]
    # return folders_id_title

    # Instead, get actual folders so we can get the v2ids.

    batched_folders_v3id = []
    batch_max = 100
    while len(folders_v3id):
        batched_folders_v3id.append(folders_v3id[:batch_max])
        del folders_v3id[:batch_max]

    folders_id_title = []
    for batch in batched_folders_v3id:
        folders_arg = ','.join(batch)
        folders = wrk.folders[folders_arg]()
        folders_id_title.extend(
            [(v2id(f), f['title']) for f in folders
             if v2id(f) != parent_folder_v2id])

    return folders_id_title

def get_folder(username, folder_v2id):
    wrk = Wrike()
    user = wrk.find_contact(username)
    folder_v3id = wrk.find_v3id(folder_v2id, 'ApiV2Folder')
    folder = wrk.folders[folder_v3id]()[0]
    return (v2id(folder), folder['title'])

def print_get_folders(username, parent_folder_v2id):
    folders_id_title = get_folders(username, parent_folder_v2id)
    for id, title in folders_id_title:
        print('%s %s' % (id, title))

def get_status(username, task_title):
    wrk = Wrike()
    user = wrk.find_contact(username)
    task = wrk.find_task_by_title(task_title, user['id'])
    return task['status']

def set_status(username, task_title, status):
    wrk = Wrike()
    user = wrk.find_contact(username)
    task = wrk.find_task_by_title(task_title, user['id'])
    wrk.tasks[task['id']].put(status=status)

dispatch_table = {
    'sync_timelog_report': lambda user, args: syncwrike('report', user, args[0]),
    'sync_timelog_prompt': lambda user, args: syncwrike('prompt', user, args[0]),
    'sync_timelog': lambda user, args: syncwrike('sync', user, args[0]),
    'find_task_v2id': lambda user, args: print(find_task_v2id(user, args[0])),
    'add_task': lambda user, args: print(add_task(user, args[0], int(args[1]))),
    'add_folder': lambda user, args: print(add_folder(user, args[0], int(args[1]))),
    'get_folders': lambda user, args: print_get_folders(user, args[0]),
    'get_folder': lambda user, args: print('%s %s' % get_folder(user, args[0])),
    'get_status': lambda user, args: print(get_status(user, args[0])),
    'set_status': lambda user, args: set_status(user, args[0], args[1]),
}

def wrike(user, command, args):
    dispatch_table[command](user, args)

if __name__ == '__main__':
    user = sys.argv[1]
    command = sys.argv[2]
    wrike(user, command, sys.argv[3:])
