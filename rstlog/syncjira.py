#! /usr/bin/python3

from __future__ import print_function
from collections import defaultdict
from rstlog.summarize import get_date_sections, pairwise
from jira import JIRA
import datetime
import os
import pickle
import re
import sys


class SyncStatus(object):
    NO_JIRA_TASK = 0
    NO_JIRA_LOG = 1
    MULTIPLE_JIRA_LOGS = 2
    LOG_SYNCED = 3
    LOG_UNSYNCED = 4

class TaskTime(object):
    def __init__(self, task_title, date, time, status=None, task_id=None, log_id=None):
        self.task_title=task_title
        self.date=date
        self.time=time
        self.sync_status=status
        self.jira_task_id = task_id
        self.jira_log_id = log_id

def find_task_by_title(jira, title):
    match = re.match("SIDP-\d+", title)
    if match is None:
        return None # raise exception?
    return jira.issue(match.group(0))

def get_task_times(date_sections):
    task_times = []
    for ds in date_sections:
        timestamps = ds.timestamps
        timestamps.sort(key=lambda ts:ts.utc)
        if len(timestamps) and not timestamps[-1].stop:
            raise Exception("Work has not stopped: %s" % ds.date)

        ds_task_times = defaultdict(datetime.timedelta)
        for begin, end in pairwise(ds.timestamps):
            if begin.stop or begin.task is None:
                continue
            ds_task_times[begin.task.title] += end.utc - begin.utc

        for title in ds_task_times:
            task_times.append(TaskTime(task_title=title,
                                       date=ds.date,
                                       time=ds_task_times[title]))
    return task_times


def jira_timestamp_to_datetime(timestamp):
    return datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.000%z')

def to_local_time(dt):
    return dt.astimezone(datetime.timezone(datetime.timedelta(hours=6)))

def jira_timestamp_to_local_date(timestamp):
    dt = to_local_time(jira_timestamp_to_datetime(timestamp))
    return datetime.datetime(dt.year, dt.month, dt.day)

def timedelta_to_seconds(td):
    # Jira records whole minutes and discards any extra seconds; 59 seconds == 0 minutes.
    minutes, seconds = divmod(td.seconds, 60)
    return minutes * 60


def match_timelogs(jira, user_id, task_times):
    task_cache = {}
    for ttm in task_times:
        if ttm.task_title not in task_cache:
            task_cache[ttm.task_title] = find_task_by_title(jira, ttm.task_title)
        task = task_cache[ttm.task_title]

        if task:
            ttm.jira_task_id = task.id
            timelogs = [log \
                        for log in jira.worklogs(task.id) \
                        if log.author.name == user_id and jira_timestamp_to_local_date(log.started) == ttm.date]

            # print(dir(jira.worklogs(task.id)[0]))
            # print(repr(jira.worklogs(task.id)[0].author.name))

            # print("timelogs:%d/%d" % (len(timelogs), len(jira.worklogs(task.id))))
            # print(task.id)
            # for log in jira.worklogs(task.id):
            #     print("  %s vs %s" % (log.author.name, user_id))
            #     print("  %s vs %s" % (jira_timestamp_to_local_date(log.started), ttm.date))

            if len(timelogs) == 1:
                ttm.jira_log_id = timelogs[0].id
                # print("%d vs %d" % (timelogs[0].timeSpentSeconds, timedelta_to_seconds(ttm.time)))
                if timelogs[0].timeSpentSeconds == timedelta_to_seconds(ttm.time):
                    ttm.sync_status = SyncStatus.LOG_SYNCED
                else:
                    ttm.sync_status = SyncStatus.LOG_UNSYNCED
            elif len(timelogs) > 1:
                ttm.sync_status = SyncStatus.MULTIPLE_JIRA_LOGS
            else:
                ttm.sync_status = SyncStatus.NO_JIRA_LOG
        else:
            ttm.sync_status = SyncStatus.NO_JIRA_TASK

def report(task_times):
    for ttm in task_times:
        if ttm.sync_status == SyncStatus.LOG_SYNCED:
            action = 'is up to date'
        elif ttm.sync_status == SyncStatus.LOG_UNSYNCED or \
             ttm.sync_status == SyncStatus.NO_JIRA_LOG:
            action = 'will be synced to %s (%s)' % \
                     (ttm.jira_task_id,
                      timedelta_to_seconds(ttm.time))
        elif ttm.sync_status == SyncStatus.NO_JIRA_TASK:
            action = 'has no single matching Jira task'
        elif ttm.sync_status == SyncStatus.MULTIPLE_JIRA_LOGS:
            action = 'has multiple time entries and will not be synced'
        else:
            action = 'unknown status'

        print('%s "%s" %s.' % (ttm.date.strftime('%Y-%m-%d'),
                               ttm.task_title,
                               action))

def synchronize(jira, task_times):
    count = 0
    for ttm in task_times:
        if ttm.sync_status == SyncStatus.LOG_UNSYNCED:
            jira.worklog(ttm.jira_task_id, ttm.jira_log_id).delete()

        if ttm.sync_status in [SyncStatus.NO_JIRA_LOG, SyncStatus.LOG_UNSYNCED]:
            wl = jira.add_worklog(ttm.jira_task_id,
                                  started=ttm.date,
                                  timeSpentSeconds=str(timedelta_to_seconds(ttm.time)))
            count += 1

    return count

def write_auth(filename, username, password):
    with open(filename, 'wb') as auth_file:
        pickle.dump((username, password), auth_file)

def read_auth(filename):
    with open(filename, 'rb') as auth_file:
        username, password = pickle.load(auth_file)
    return username, password

def syncjira(command,
             user,
             log_filename):
    jira = JIRA(server='https://jira.allplan.com',
                basic_auth=read_auth(os.path.expanduser('~/.jira_basic_auth')))

    sync_date_filename = os.path.join(os.path.expanduser('~'),
                                      '.rstlog_sync_date')

    with open(log_filename, 'r') as log_file:
        try:
            with open(sync_date_filename) as sync_date_file:
                sync_date_str = sync_date_file.read()
            sync_date = datetime.datetime.strptime(sync_date_str, '%Y-%m-%d')
        except (IOError, ValueError):
            sync_date = None

        date_sections = get_date_sections(log_file.read(),
                                          date_start=sync_date, date_end=None)
        task_times = get_task_times(date_sections)
        match_timelogs(jira, user, task_times)
        if command == 'report' or command == 'prompt':
            report(task_times)

        ok = False
        if command == 'prompt':
            prompt='Ok? (Y/N): '
            try: # python2
                ok = raw_input(prompt) == 'Y'
            except NameError: # python3
                ok = input(prompt) == 'Y'
            except EOFError:
                pass

        if ok or command == 'sync':
            count = synchronize(jira, task_times)
            print('Synced %d task times.' % count)
            with open(sync_date_filename, 'w') as sync_date_file:
                sync_date_file.write(
                    datetime.datetime.today().strftime('%Y-%m-%d'))

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print('Usage: syncjira.py [report|sync|prompt] username ~/path/to/your/log.rst')
        sys.exit(0)

    command = sys.argv[1]
    user = sys.argv[2]
    log_filename = sys.argv[3]

    syncjira(command, user, log_filename)
