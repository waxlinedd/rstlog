#! /usr/bin/python

from __future__ import print_function
from collections import defaultdict
from docutils.parsers import rst
from functools import total_ordering
import docutils.nodes
import docutils.utils
import datetime
import os
import re
import sys


# week_start_day: Monday = 0, Tuesday = 1, etc.
# direction: > 0 chronological order
#            < 0 reverse chronological

def group_by_week(date_list, key=lambda x:x, week_start_day=0, direction=1):
    date_list.sort(key=key)

    sentinel = None
    week = []

    if direction >= 0:
        past_sentinel = lambda date, sentinel: sentinel and date > sentinel
        get_sentinel = lambda date, week_day:date + datetime.timedelta(6-week_day, 0, 0)
    else:
        past_sentinel = lambda date, sentinel: sentinel and date < sentinel
        get_sentinel = lambda date, week_day: date - datetime.timedelta(week_day, 0, 0)
        date_list.reverse()

    for item in date_list:
        date = key(item)
        if past_sentinel(date, sentinel):
            yield week
            week = []
            sentinel = None

        if sentinel is None:
            normalized_week_day = (date.weekday() - week_start_day) % 7
            sentinel = get_sentinel(date, normalized_week_day)

        week.append(item)

    if week:
        yield week


def hours_and_minutes(td):
    hours, remainder = divmod(td.seconds, 60 * 60)
    minutes, seconds = divmod(remainder, 60)
    if seconds >= 30:
        minutes += 1
    if minutes >= 60:
        hours +=1
        minutes -= 60
    hours += td.days * 24
    return '%d:%02d' % (hours, minutes)

def get_date_sections(log, date_start=None, date_end=None):
    '''
    Parse reStructuredText, returning a list of DateSections.

    :param str log: reStructuredText log string
    :param datetime date_start: Start of interval to return
    :param datetime date_end: End of interval to return
    :return: List of DateSections
    '''
    #document = parse_rst(unicode(log, encoding='utf-8')) # python2?
    document = parse_rst(log)
    # testvisitor = TestVisitor(document)
    # document.walkabout(testvisitor)
    visitor = DateSectionVisitor(document)
    document.walk(visitor)
    def date_ok(date):
        if date_start and date < date_start:
            return False
        if date_end and date > date_end:
            return False
        return True
    date_sections = filter(lambda x:date_ok(x.date), visitor.date_sections)
    return date_sections

def summarize(log, summary_file, date_start=None, date_end=None):
    date_sections = get_date_sections(log, date_start, date_end)

    for week in group_by_week(date_sections,
                              key=lambda ii: ii.date,
                              week_start_day=5,
                              direction=-1):
        week_total_hours = datetime.timedelta()
        week_summary = ''
        for section in week:
            date_summary, date_total_hours = summarize_date_section(section)
            week_summary += date_summary + "\n"
            week_total_hours += date_total_hours


        summary_file.write('Week hours: %s\n\n' % hours_and_minutes(week_total_hours))
        summary_file.write(week_summary)
        summary_file.write('-' * 80)
        summary_file.write('\n\n')

def parse_rst(text):
    parser = rst.Parser()
    components = (rst.Parser,)
    settings = docutils.frontend.OptionParser(components=components).get_default_values()
    document = docutils.utils.new_document('<rst-doc>', settings=settings)
    parser.parse(text, document)
    return document

class TestVisitor(docutils.nodes.NodeVisitor):
    def __init__(self, document):
        self.indent=-1
        docutils.nodes.NodeVisitor.__init__(self, document)

    def dispatch_visit(self, node):
        self.indent += 1
        docutils.nodes.NodeVisitor.dispatch_visit(self, node)

    def dispatch_departure(self, node):
        self.indent -= 1

    def visit_section(self, node):
        """Called for section nodes."""

        foo = datetime.datetime.strptime(node.children[0].rawsource, '%Y-%m-%d')
        print("date:%s"%foo)

        # print(node.__class__.__name__)
        # for child in node.children:
        #     print('   ', child.__class__.__name__)

        #print("Section: ", node.children[1].__dict__['attributes'])

    # def visit_header(self, node):
    #     """Called for header nodes."""
    #     print("Header: ", node.title)

    def unknown_visit(self, node):
        """Called for all other node types."""
        print("%s%s" % ('  '*self.indent, node.__class__.__name__))


class DateSectionVisitor(docutils.nodes.NodeVisitor):
    def __init__(self, document):
        self.date_sections = []
        docutils.nodes.NodeVisitor.__init__(self, document)


    def visit_section(self, node):
        """Called for section nodes."""
        date = None
        try:
            date = datetime.datetime.strptime(node.children[0].rawsource, '%Y-%m-%d')
            self.date_sections.append(DateSection(date))
        except:
            pass

        if not date and len(self.date_sections):
            subsection = SubSection(node.children[0].rawsource)
            self.date_sections[-1].subsections.append(subsection)

        #print("Section: ", node.children[1].__dict__['attributes'])

    def visit_TimestampNode(self, node):
        if not len(self.date_sections):
            raise Exception("Timestamp outside of Date Section.")
        date_section = self.date_sections[-1]
        if len(date_section.subsections):
            task = date_section.subsections[-1]
        else:
            task = None

        utc_str = node.attributes['utc_str']
        stop = 'stop' in node.attributes
        date_section.timestamps.append(Timestamp(utc_str, stop, task))

    # def visit_header(self, node):
    #     """Called for header nodes."""
    #     print("Header: ", node.title)

    def unknown_visit(self, node):
        """Called for all other node types."""
        #print("%s%s" % ('  '*self.indent, node.__class__.__name__))


class TimestampDirective(rst.Directive):
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = False
    option_spec = {'stop':lambda _:True}
    has_content = False

    def run(self):
        if self.content:
            raise self.warning('%s directive has content.' % self.name)
        utc_str = self.arguments[0]
        timestamp_node = TimestampNode(utc_str=utc_str,**self.options)
        return [timestamp_node]

rst.directives.register_directive('timestamp', TimestampDirective)

class TimestampNode(docutils.nodes.Element):
    pass


class DateSection(object):
    def __init__(self, date):
        self.date = date
        self.subsections = []
        self.timestamps = []
        self.hours = None

@total_ordering
class SubSection(object):
    def __init__(self, title):
        self.title = title
        self.hours = None

    def __lt__(self, rhs):
        return self.title < rhs.title

    def __eq__(self, rhs):
        return self.title == self.title

@total_ordering
class Timestamp(object):
    def __init__(self, utc_str, stop=False, task=None):
        self.task=task
        self.stop = stop
        self.utc_str = utc_str
        self.utc = timestamp_to_datetime(utc_str)

    def __lt__(self, rhs):
        return self.utc < rhs.utc

    def __eq__(self, rhs):
        return self.utc == rhs.utc


def timestamp_to_datetime(timestamp):
    # https://stackoverflow.com/questions/969285/how-do-i-translate-an-iso-8601-datetime-string-into-a-python-datetime-object
    # This regex removes all colons and all
    # dashes EXCEPT for the dash indicating + or - utc offset for the timezone
    conformed_timestamp = re.sub(r"[:]|([-](?!((\d{2}[:]\d{2})|(\d{4}))$))", '', timestamp)

    # Split on the offset to remove it. Use a capture group to keep the delimiter
    split_timestamp = re.split(r"[+|-]",conformed_timestamp)
    main_timestamp = split_timestamp[0]
    if len(split_timestamp) == 3:
        sign = split_timestamp[1]
        offset = split_timestamp[2]
    else:
        sign = None
        offset = None

    # Generate the datetime object without the offset at UTC time
    output_datetime = datetime.datetime.strptime(main_timestamp +"Z", "%Y%m%dT%H%M%SZ" )
    if offset:
        # Create timedelta based on offset
        offset_delta = datetime.timedelta(hours=int(sign+offset[:-2]), minutes=int(sign+offset[-2:]))

        # Offset datetime with timedelta
        output_datetime = output_datetime + offset_delta
    return output_datetime


def summarize_date_section(date_section):
    summary = ''
    timestamps = date_section.timestamps
    if not timestamps:
        return '', datetime.timedelta()
    timestamps.sort(key=lambda ts:ts.utc)
    if len(timestamps) and not timestamps[-1].stop:
        raise Exception("Work has not stopped: %s" % date_section.date)

    # print date
    summary += date_section.date.strftime('%Y-%m-%d\n')

    # print work intervals
    start_ts=None
    for ts in timestamps:
        if start_ts is None:
            start_ts = ts
        if ts.stop:
            summary += ' '*11
            summary += start_ts.utc.strftime("%H:%M")
            summary += ts.utc.strftime("-%H:%M\n")
            start_ts = None


    # print total time worked
    working_intervals = []
    current_interval = []
    for ts in timestamps:
        current_interval.append(ts)
        if ts.stop:
            if len(current_interval) == 1:
                raise Exception("Worked stopped before starting: %s" %  date_section.date)
            working_intervals.append(current_interval)
            current_interval = []

    total_time = reduce(lambda a, x: a + (x[-1].utc-x[0].utc),
                        working_intervals, datetime.timedelta())
    summary += ' '*11
    summary += 'Total hours: %s\n' % (total_time)

    # print tasks
    task_times = defaultdict(datetime.timedelta)
    for begin, end in pairwise(date_section.timestamps):
        if begin.stop or begin.task is None:
            continue
        task_times[begin.task] += end.utc - begin.utc

    for task, task_time in task_times.items():
        summary += '%s%s: %s\n' % (' '*11, task.title, task_time)

    return summary, total_time


def pairwise(seq, circularly=False):
    '''A generator to allow iteration over a sequence in overlapping
    pairs, circularly.  For example: (1,2,3) -> (1,2), (2,3), (3,1)'''
    ii = iter(seq)
    sentinel = object()
    first = prev = item = next(ii, sentinel)
    if item is sentinel:
        return
    for item in ii:
        yield prev, item
        prev = item
    if circularly and item is not first:
        yield item, first


if __name__ == '__main__':

    if len(sys.argv) > 1:
        log_filename = sys.argv[1]
    else:
        log_filename = os.path.join(os.getenv('HOME'), 'Documents', 'log', 'log.rst')

    if len(sys.argv) > 2:
        summary_filename = sys.argv[2]
    else:
        summary_filename = os.path.join(os.getenv('HOME'), 'Documents', 'log', 'log_summary.txt')

    with open(log_filename, 'r') as log_file, open(summary_filename, 'w') as summary:
        summarize(log_file.read(), summary)
    print("Log summary written to %s" % summary_filename)
