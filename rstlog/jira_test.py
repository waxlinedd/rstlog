import datetime
import jira
import os
import pickle
import rstlog

def test_jira(aj):
    # try:
    #     foo = aj.issue('foobarbaz')
    # except Exception as err:
    #     print(err)

    issue = aj.issue('SIDP-275176')

    print(issue.fields.project.key)
    print(issue.fields.issuetype.name)
    print(issue.fields.reporter.displayName)
    print(dir(issue.fields))
    #ff = datetime.datetime.strptime('2024-01-30T14:42:01.000+0100', '%Y-%m-%dT%H:%M:%S.000%z')

    # aj.close()

def test_worklog():
    aj = jira.JIRA(server='https://jira.allplan.com',
                   basic_auth=read_auth(os.path.expanduser('~/.jira_basic_auth')))

    issue = aj.issue('SIDP-274868')
    print('Issue')
    print(issue.id)

    aj.add_worklog('SIDP-274868', started=datetime.datetime(2024, 1, 30, 13, 42, 1), timeSpentSeconds='61')

    logs = aj.worklogs('SIDP-274868')
    # print(repr(logs))
    # print(dir(logs[0]))
    # print(logs[0].author)
    # print(logs[0].created)
    # print(logs[0].timeSpent)
    # print(logs[0].timeSpentSeconds)
    print(logs[0].raw)
    print(repr(logs[0].started))
    for log in logs:
         log.delete()

    daxline = aj.user('daxline')
    print(daxline.raw)


def write_auth(filename, username, password):
    with open(filename, 'wb') as auth_file:
        pickle.dump((username, password), auth_file)

def read_auth(filename):
    with open(filename, 'rb') as auth_file:
        username, password = pickle.load(auth_file)
    return username, password

def test_find_task_by_title(aj):
    task = rstlog.syncjira.find_task_by_title(aj, 'SIDP-274868')
    print('%s:%s' % (task.id, task))

test_log = '''
2024-01-04
==========
.. timestamp:: 2024-01-04T18:33:28-06:00
    :stop:

Catching up
-----------
.. timestamp:: 2024-01-04T10:33:28-06:00

SIDP-274868: Convert to Qt: Std. Fabricator Connections > Joist Connection Settings
-----------------------------------------------------------------------------------
.. timestamp:: 2024-01-04T12:15:35-06:00

2024-01-16
==========
.. timestamp:: 2024-01-16T16:45:29-06:00
    :stop:

SIDP-274868: Convert to Qt: Std. Fabricator Connections > Joist Connection Settings
-----------------------------------------------------------------------------------
.. timestamp:: 2024-01-16T08:45:29-06:00

2024-01-17
==========
.. timestamp:: 2024-01-17T17:00:10-06:00
    :stop:

SIDP-274868: Convert to Qt: Std. Fabricator Connections > Joist Connection Settings
-----------------------------------------------------------------------------------
.. timestamp:: 2024-01-17T09:00:10-06:00

'''

def test_get_date_sections(aj):
    print(rstlog.summarize.get_date_sections(test_log))

def test_get_task_times(aj):
    date_sections = rstlog.summarize.get_date_sections(test_log)
    print(rstlog.syncjira.get_task_times(date_sections))

def test_match_timelogs(aj):
    date_sections = rstlog.summarize.get_date_sections(test_log)
    task_times = rstlog.syncjira.get_task_times(date_sections)
    rstlog.syncjira.match_timelogs(aj, 'daxline', task_times)
    for ttm in task_times:
        print('title:%s task_id:%s log_id:%s sync_status:%s' % (ttm.task_title, ttm.jira_task_id, ttm.jira_log_id, ttm.sync_status))

if __name__ == '__main__':
    ## Does not seem to work:
    #
    # with open(os.path.expanduser('~/.rstlog_jira_token')) as token_file:
    #     token = token_file.readlines()
    # aj = jira.JIRA(server='https://jira.allplan.com',
    #                token_auth=token)

    aj = jira.JIRA(server='https://jira.allplan.com',
                   basic_auth=read_auth(os.path.expanduser('~/.jira_basic_auth')))
    # test_jira(aj)
    # test_worklog(aj)
    # test_find_task_by_title(aj)
    # test_get_date_sections(aj)
    # test_get_task_times(aj)
    test_match_timelogs(aj)
