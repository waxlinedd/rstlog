#! /usr/bin/python

from __future__ import print_function
from collections import defaultdict
from summarize import get_date_sections, pairwise
from wryke.wrike import Wrike, timedelta_to_hours
import datetime
import datetime
import os
import sys


class SyncStatus(object):
    NO_WRIKE_TASK = 0
    NO_WRIKE_LOG = 1
    MULTIPLE_WRIKE_LOGS = 2
    LOG_SYNCED = 3
    LOG_UNSYNCED = 4

class TaskTime(object):
    def __init__(self, task_title, date, time, status=None, task_id=None, log_id=None):
        self.task_title=task_title
        self.date=date
        self.time=time
        self.sync_status=status
        self.wrike_task_id = task_id
        self.wrike_log_id = log_id

def get_task_times(date_sections):
    task_times = []
    for ds in date_sections:
        timestamps = ds.timestamps
        timestamps.sort(key=lambda ts:ts.utc)
        if len(timestamps) and not timestamps[-1].stop:
            raise Exception("Work has not stopped: %s" % ds.date)

        ds_task_times = defaultdict(datetime.timedelta)
        for begin, end in pairwise(ds.timestamps):
            if begin.stop or begin.task is None:
                continue
            ds_task_times[begin.task.title] += end.utc - begin.utc

        for title in ds_task_times:
            task_times.append(TaskTime(task_title=title,
                                       date=ds.date,
                                       time=ds_task_times[title]))
    return task_times

def match_timelogs(wrike, user_id, task_times):
    task_cache = {}
    for ttm in task_times:
        if ttm.task_title not in task_cache:
            task_cache[ttm.task_title] = wrike.find_task_by_title(ttm.task_title,
                                                                  user_id)
        task = task_cache[ttm.task_title]

        if task:
            ttm.wrike_task_id = task['id']

            # timelogs = getattr(wrike.tasks, task['id']).timelogs(
            timelogs = wrike.tasks[task['id']].timelogs(
                me=True,
                trackedDate='{"equal":"%s"}' % ttm.date.strftime('%Y-%m-%d'))
            if len(timelogs) == 1:
                ttm.wrike_log_id = timelogs[0]['id']
                if str(timelogs[0]['hours']) == str(timedelta_to_hours(ttm.time)):
                    ttm.sync_status = SyncStatus.LOG_SYNCED
                else:
                    ttm.sync_status = SyncStatus.LOG_UNSYNCED
            elif len(timelogs) > 1:
                ttm.sync_status = SyncStatus.MULTIPLE_WRIKE_LOGS
            else:
                ttm.sync_status = SyncStatus.NO_WRIKE_LOG
        else:
            ttm.sync_status = SyncStatus.NO_WRIKE_TASK

def report(task_times):
    for ttm in task_times:
        if ttm.sync_status == SyncStatus.LOG_SYNCED:
            action = 'is up to date'
        elif ttm.sync_status == SyncStatus.LOG_UNSYNCED or \
             ttm.sync_status == SyncStatus.NO_WRIKE_LOG:
            action = 'will be synced to %s (%s)' % \
                     (ttm.wrike_task_id,
                      timedelta_to_hours(ttm.time))
        elif ttm.sync_status == SyncStatus.NO_WRIKE_TASK:
            action = 'has no single matching Wrike task'
        elif ttm.sync_status == SyncStatus.MULTIPLE_WRIKE_LOGS:
            action = 'has multiple time entries and will not be synced'
        else:
            action = 'unknown status'

        print('%s "%s" %s.' % (ttm.date.strftime('%Y-%m-%d'),
                               ttm.task_title,
                               action))

def synchronize(wrike, task_times):
    count = 0
    for ttm in task_times:
        if ttm.sync_status == SyncStatus.NO_WRIKE_LOG:
            wrike.tasks[ttm.wrike_task_id].timelogs.post(
                hours=timedelta_to_hours(ttm.time),
                trackedDate=ttm.date.strftime('%Y-%m-%d'))
            count += 1
        elif ttm.sync_status == SyncStatus.LOG_UNSYNCED:
            wrike.timelogs[ttm.wrike_log_id].put(
                hours=timedelta_to_hours(ttm.time))
            count += 1

    return count

def syncwrike(command,
              user,
              log_filename):
    wrike = Wrike()
    user_id = wrike.find_contact(user)['id']
    sync_date_filename = os.path.join(os.path.expanduser('~'),
                                      '.rstlog_sync_date')

    with open(log_filename, 'r') as log_file:
        try:
            with open(sync_date_filename) as sync_date_file:
                sync_date_str = sync_date_file.read()
            sync_date = datetime.datetime.strptime(sync_date_str, '%Y-%m-%d')
        except IOError, ValueError:
            sync_date = None

        date_sections = get_date_sections(log_file.read(),
                                          date_start=sync_date, date_end=None)
        task_times = get_task_times(date_sections)
        match_timelogs(wrike, user_id, task_times)
        if command == 'report' or command == 'prompt':
            report(task_times)

        ok = False
        if command == 'prompt':
            prompt='Ok? (Y/N): '
            try: # python2
                ok = raw_input(prompt) == 'Y'
            except NameError: # python3
                ok = input(prompt) == 'Y'
            except EOFError:
                pass

        if ok or command == 'sync':
            count = synchronize(wrike, task_times)
            print('Synced %d task times.' % count)
            with open(sync_date_filename, 'w') as sync_date_file:
                sync_date_file.write(
                    datetime.datetime.today().strftime('%Y-%m-%d'))

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print('Usage: syncwrike.py [report|sync|prompt] you@nowhere.net ~/path/to/your/log.rst')
        sys.exit(0)

    command = sys.argv[1]
    user = sys.argv[2]
    log_filename = sys.argv[3]

    syncwrike(command, user, log_filename)
