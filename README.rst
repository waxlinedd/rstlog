======================
 reStructuredText Log
======================

Overview
========

rstlog is for taking daily project notes and logging time.  Times logged
in rstlog can be synchronized to timelogs in Wrike.

rstlog helps you:

* Keep a work log in the reStructuredText format
* Track time spent on tasks with minimal keystrokes
* Generate summary of work log for management
* Insert Wrike id of a task based on the task title
* Synchronize tracked time with Wrike

Integration with Emacs is provided, but rstlog is mostly written in
python, and can be used from the commandline.  Integrations with other
editors can be created to the extent that other editors are extendable.



Install
=======

Install debian packages:

* python-requests
* python-urllib3
* python-certifi

Clone repositories
`requesto <https://waxlinedd@bitbucket.org/waxlinedd/requesto>`_,
`wryke <https://waxlinedd@bitbucket.org/waxlinedd/wryke>`_, and
`rstlog <https://waxlinedd@bitbucket.org/waxlinedd/rstlog>`_::

  cd ~/src
  git clone git@bitbucket.org:waxlinedd/requesto.git
  git clone git@bitbucket.org:waxlinedd/wryke.git
  git clone git@bitbucket.org:waxlinedd/rstlog.git




In Wrike:

#. Browse to your personal Wrike folder and create three new folders: "Bugzilla", "Miscellaneous", "Projects"
#. Create two new tasks "Consulations" and "Meetings", and assign them to yourself.
#. Note the permalink id numbers of the folders and tasks just created for use in emacs setup.
#. Note the permalink id of your personal Wrike folder.
#. Click on your account menu in the upper right corner.
#. Click on "Apps & Integrations" in this menu.
#. Click on "API" on the left menu.
#. Create a new app named "rstlog" (or whatever you like).
#. In the App configuration for this app, scroll to the bottom and click "Create v4 Token".
#. Copy the token to a new file in your home directory: ~/.rstlog_wrike_token
#. Good job!


Add the following to your emacs init.el with paths and email
address customized as needed::

    ;; rstlog
    (add-to-list 'load-path "~/src/rstlog/")
    (load-library "rstlog")

    (setq log-file (format "/home/%s/Documents/log/log.rst" (getenv "USER")))
    (setq log-summary (format "/shared/home/%s/Documents/log_summary.txt" (getenv "USER")))
    (setq log-mirror (format "/shared/home/%s/Documents/log.rst" (getenv "USER")))
    (setq wrike-username <your email address>)

    (setq wrike-projects-folder-v2id <your wrike folder id>)
    (setq wrike-pr-folder-v2id <your wrike folder id>)
    (setq wrike-misc-folder-v2id <your wrike folder id>)
    (setq wrike-consultation-task-v2id <your wrike task id>)
    (setq wrike-meeting-task-v2id <your wrike task id>)
    (setq wrike-home-folder-v2id <your wrike folder id>)
    (setq wrike-current-project-folder-v2id wrike-home-folder-v2id)

    (setq homedir (expand-file-name "~"))
    (setenv "PYTHONPATH"
          (concat homedir "/src/requesto:"
                  homedir "/src/wryke:"
                  homedir "/src/rstlog:"
                  (getenv "PYTHONPATH")))

Optionally::

    ;; Start cycling new adornments to one below the most recent adornment.
    (setq-default rst-new-adornment-down t)

    ;; Make "M-x indent-rigidly-right-to-tabstop" indent in 4 column
    ;; increments rather than 8.
    (setq tab-stop-list '(0 4))

    ;; Used for wrike-user-name
    (setq user-mail-address (concat (user-login-name) "@sds2.com"))

Emacs Tutorial
==============

Open your work log::

  C-c ll
  M-x open-log

Start a new section for the day::

  C-c md
  M-x mark-date-section

A "stop" timestamp is automatically added to the date section at +8 hours.
Timestamps in a section indicate the time work started/stopped for that
section.  There is no need to add "stop" timestamps when switching from
one task to another, only when you are stopping work altogether.  Switch
tasks by adding a timestamp to the task you are switching to.

Add a new wrike task to the current wrike folder::

  C-c wat
  M-x rstlog-add-wrike-current-project-task

By default, your current wrike folder is your home wrike folder.  New
tasks will be added to the current wrike folder. To see which folder is
the current wrike folder::

  C-c wpf
  M-x rstlog-print-current-wrike-folder

To set the current wrike folder::

  C-c wsf
  M-x rstlog-set-current-wrike-folder

To create a new wrike folder in "Projects" and set it as the current
wrike folder::

  C-c waf
  M-x rstlog-add-wrike-folder

Synchronize logged times with Wrike::

  C-c wsy
  M-x sync-wrike

Open your work log summary::

  C-c ls
  M-x open-log-summary

Start a new task that is not in Wrike::

  C-c ms
  M-x mark-section

Add a timestamp::

  C-c mb
  M-x mark-begin-work

Add a stop timestamp::

  C-c me
  M-x mark-end-work

If your section title is identical to the title of a task in Wrike, add
the Wrike task id to the beginning of the section title::

  C-c wt
  M-x insert-wrike-task-id

